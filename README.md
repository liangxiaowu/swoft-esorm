# swoft-esorm

#### 介绍
基于swoft的elasticsearch组件方式

#### 安装
```txt
composer require wunder/esorm
```

#### bean配置
```php
'esorm'      => [
   'class'   =>\Wunder\Esorm\Client::class,
   'scheme'  => "http",             
   'host'    => '127.0.0.1',           
   'port'    => 19200,          
   'user'    => 'username',
   'pass'    => '123456 ',
],
'esorm.pool' => [
    'class'  => \Wunder\Esorm\Pool::class,
    'client' => bean('esorm'),
    'minActive'   => 10,
    'maxActive'   => 1000,
    'maxWait'     => 0,
    'maxWaitTime' => 5,
    'maxIdleTime' => 40,
]
```
Esorm
- class: 当前配置驱动类`\Wunder\Esorm\Client::class`。
- shceme: 请求方式。
- hsot: 连接地址 默认`127.0.0.1`。
- user: 用户名。
- pass: 密码。
- port: 端口 默认`19200`。

Esorm.pool
- class 连接池驱动类 一般不用改，如果需要可以继承写成自己的 Pool 类也是可以的。
- client 指定当前连接使用的`client`。
- minActive 连接池需要维持的连接数。
- maxActive 连接池最大保持的连接数。
- maxWait 连接池最多等待连接数, 如果没有限制为0(默认)。
- maxWaitTime 连接最大等待时间，单位秒，如果没有限制为0(默认)。
- maxIdleTime 连接最大空闲时间，单位秒。

#### 集群方式
```php
'esorm'      => [
   'class'   =>\Wunder\Esorm\Client::class,
   'scheme'  => "http",             
   'hosts'    => [
        [
           'host'    => '127.0.0.1',           
           'port'    => 19200,          
           'user'    => 'username',
           'pass'    => '123456 ',
        ],
        [
           'host'    => '127.0.0.1',           
           'port'    => 19201,          
           'user'    => 'username',
           'pass'    => '123456 ',
        ],   
    ]   
],
'esorm.pool' => [
    'class'  => \Wunder\Esorm\Pool::class,
    'client' => bean('esorm'),
    'minActive'   => 10,
    'maxActive'   => 1000,
    'maxWait'     => 0,
    'maxWaitTime' => 5,
    'maxIdleTime' => 40,
]
```
- hsots: 多个节点配置。

#### 查询器
##### 声明查询的索引名字
```php
$model = Esorm::index("index_name");
```
##### 查询方法
```
$model->mustTerm("key1","value1");
$model->mustNotTerm("key2","value2");
$model->shouldTerm("key3","value3")->shouldTerm("key4","value4");

```
`mustTerm()`方法: 筛选`key1 = value1`条件
```json
elasticsearch：
{
  "query": {
    "bool": {
      "must": [
        {
          "term": {
            "key1": {
              "value": "value1"
            }
          }
        }
      ]
    }
  }
}
```
`mustTerm()`方法: 筛选`key2 != value2`条件
```json
elasticsearch：
{
  "query": {
    "bool": {
      "must_not": [
        {
          "term": {
            "key2": {
              "value": "value2"
            }
          }
        }
      ]
    }
  }
}
```
`shouldTerm()`方法:筛选`key3 = value3 or key4 = value4`
```json
elasticsearch：
{
  "query": {
    "bool": {
      "should": [
        {
          "term": {
            "key3": {
              "value": "value3"
            }
          }
        },
        {
          "term": {
            "key4": {
              "value": "value4"
            }
          }
        }
      ]
    }
  }
}
```
##### 嵌套查询
支持多层的嵌套查询
```php
Esorm:
 $model->MustTerm("key1","key1")->newMust(function (Builder $builder){
    return $builder->mustTerm("key2", "value2");
 })
elasticsearch：
{
  "query": {
    "bool": {
      "must": [
        {
          "term": {
            "key1": {
              "value": "value1"
            }
          }
        },
        {
          "bool": {
            "must": [
              {
                "term": {
                  "key2": {
                    "value": "value2"
                  }
                }
              }
            ]
          }
        }
      ]
    }
  }
}
```
##### 条件筛选
- `newMust($closure)`:`must`嵌套查询
- `mustTerm(string $key, string $value="")`:`must`下的`term`筛选查询
- `mustTerms(string $key, $value=[])`:`must`下的`terms`筛选查询
- `mustMatch(string $key, string $value)`:`must`下的`match`筛选查询
- `mustMatchPhrase(string $key, string $value)`:`must`下的`match_phrase`筛选查询
- `mustRange(string $key, array $value)`:`must`下的`range`筛选查询

- `newMustNot($closure)`:`must_not`嵌套查询
- `mustNotTerm(string $key, string $value="")`:`must_not`下的`term`筛选查询
- `mustNotTerms(string $key, $value=[])`:`must_not`下的`terms`筛选查询
- `mustNotMatch(string $key, string $value)`:`must_not`下的`match`筛选查询
- `mustNotMatchPhrase(string $key, string $value)`:`must_not`下的`match_phrase`筛选查询
- `mustNotRange(string $key, array $value)`:`must_not`下的`range`筛选查询

- `newShould($closure)`:`should`嵌套查询
- `shouldTerm(string $key, $value="")`:`should`下的`term`筛选查询
- `shouldTerms(string $key, $value=[])`:`should`下的`terms`筛选查询
- `shouldMatch(string $key, string $value)`:`should`下的`match`筛选查询
- `shouldMatchPhrase(string $key, string $value)`:`should`下的`match_phrase`筛选查询
- `shouldRange(string $key, array $value)`:`should`下的`range`筛选查询

##### 查询结果
- `get(array $source = [])`:获取所有结果
- `get(array $source = [])`:获取分页结果
- `first(array $source=[])`:获取单条数据
- ...

##### 分页
- `size(int $size)`: 查询的数量
- `form(int $form)`: 查询起始数量
- `paginate(int $page, int $size)`: 分页查询，`page`:页码,`size`:页数:

##### 其他
- `source(array $source)`: 返回指定字段
- `sort(array $sorts)`: 排序

##### 参数说明
|参数|说明|
|:----|:---|
|closure|嵌套闭包函数|
|key|字段名|
|value|查询匹配值|

#### 更新
- 查询器
- 创建索引（更新中）
- 创建文档（更新中）
- 修改文档（更新中）
- model模型（更新中）
