<?php

declare(strict_types=1);
namespace Wunder\Esorm\Handler;

use GuzzleHttp\Ring\Core;
use GuzzleHttp\Ring\Exception\RingException;
use GuzzleHttp\Ring\Future\CompletedFutureArray;
use Swoole\Coroutine;
use Swoole\Coroutine\Http\Client;

/**
 * Http handler that uses Swoole Coroutine as a transport layer.
 */
class CoroutineHandler
{
    protected $options;

    public function __construct($options = [])
    {
        $this->options = $options;
    }

}
