<?php


namespace Wunder\Esorm\Handler;

use Swoft\Bean\Annotation\Mapping\Inject;

/**
 * Class Model
 * @package Wunder\Esorm\Handler
 */
class Model
{
    /**
     * index
     * @var string
     */
    protected $index;

    private $flag = "";

    public static function query():Collection
    {
        return (new Collection())->query();
    }

    public function create()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }

}
