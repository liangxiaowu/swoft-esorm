<?php


namespace Wunder\Esorm\Handler;


interface ModelInterface
{

    public function getIndex();

    public function setIndex();

}
