<?php


namespace Wunder\Esorm\Handler;

use Elasticsearch\Client;
use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * @Bean()
 * Class ElasticSearch
 * @package Wunder\Esorm\Handler
 */

class ElasticSearch
{

    /**
     * @var Client
     */
    private $Client;
}
