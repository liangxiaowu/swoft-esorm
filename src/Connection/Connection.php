<?php


namespace Wunder\Esorm\Connection;

use Elasticsearch\ClientBuilder;
use Swoft\Bean\Annotation\Mapping\Inject;
use Swoft\Bean\BeanFactory;
use Swoft\Connection\Pool\AbstractConnection;
use Swoft\Connection\Pool\Contract\PoolInterface;
use Wunder\Esorm\Client;
use Swoft\Bean\Annotation\Mapping\Bean;
use Wunder\Esorm\Handler\Collection;
use Wunder\Esorm\Handler\CoroutineHandler;

/**
 * Class Connection
 *
 * @Bean(scope=Bean::PROTOTYPE)
 *
 * @package Wunder\Esorm\Connection
 */
class Connection extends AbstractConnection
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @Inject()
     * @var Collection
     */
    public $collection;

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param int $lastTime
     */
    public function setLastTime(int $lastTime = null): void
    {
        if (is_null($lastTime)) {
            $lastTime = time();
        }
        $this->lastTime = $lastTime;
    }

    /**
     * @param PoolInterface $pool
     */
    public function setPool(PoolInterface $pool): void
    {
        $this->pool = $pool;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): void
    {
        $this->client = $client;
    }

    public function create():void
    {
        $hosts = $this->client->getHosts();
        $retries = $this->client->getRetries();
        $timeout = $this->client->getTimeout();
        $user = $this->client->getUser();
        $pass = $this->client->getPass();

        // hosts
        if(empty($hosts)){
            $hosts[] = [
                "host"=>$this->client->getHost(),
            ];
        }

        $hostGroup = [];
        foreach ($hosts as $host){
            $host['host'] = $host['host'] ?? $this->client->getHost();
            $host['port'] = $host['port'] ?? $this->client->getPort();
            $host['scheme'] = $host['scheme'] ?? $this->client->getScheme();
            $host['path'] = $host['path'] ?? $this->client->getPath();
            $host['user'] = $host['user'] ?? $user;
            $host['pass'] = $host['pass'] ?? $pass;
            $hostGroup[] = $host;
        }


        $timeout = $this->client->getTimeout();
        $options = [];
        if ($timeout > 0){
            $options['timeout'] = $timeout;
        }

        $handler = new CoroutineHandler($options);
        $builder = ClientBuilder::create()
//            ->setHandler($handler)
            ->setRetries($retries);
        if (!empty($ssl)) {
            $builder->setSSLVerification($ssl);
        }
        $builder->setHosts($hostGroup);
        $elasticsearch = $builder->build();
        $this->collection->setClient($elasticsearch);
    }

    /**
     * close
     */
    public function close(): void
    {
    }

    /**
     * reconnect
     *
     * @return bool
     */
    public function reconnect(): bool
    {
    }

    /**
     * release
     *
     * @param bool $force
     */
    public function release(bool $force = false): void
    {
        /* @var ConnectionManager $conManager */
        $conManager = BeanFactory::getBean(ConnectionManager::class);
        $conManager->releaseConnection($this->id);

        parent::release($force);
    }

}
