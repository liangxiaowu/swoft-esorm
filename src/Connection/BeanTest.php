<?php

namespace Wunder\Esorm\Connection;

use Swoft\Bean\Annotation\Mapping\Bean;

/**
 * Class BeanTest
 * @package Wunder\Esorm\Connection
 * @Bean()
 */
class BeanTest
{

    public function getId()
    {
        return "12";
    }

}
