<?php


namespace Wunder\Esorm\Eloquent;

/**
 * Trait Should
 * @package Wunder\Esorm\Eloquent
 */
trait Should
{

    public function newShould($type, $key, $value)
    {
        if($type instanceof Closure ){
            $bu = new Builder();
            $builder = $condition($bu);
            $this->should()->where($builder->getQuery());
        }else{
            $this->should()->where($type, $key, $value);
        }
        return $this;
    }

    public function shouldTerm($key, $value="")
    {
        $this->should()->term( $key, $value);
        return $this;
    }

    public function shouldTerms($key, $value=[])
    {
        $value = (array)$value;
        $this->should()->terms($key, $value);
        return $this;
    }

    public function shouldMatch(string $key, string $value)
    {
        $this->should()->match($key, $value);
        return $this;
    }
    public function shouldMatchPhrase(string $key, string $value)
    {
        $this->should()->matchPhrase($key, $value);
        return $this;
    }

    public function shouldRange($key, array $value)
    {
        $this->should()->range($key, $value);
        return $this;
    }

}
