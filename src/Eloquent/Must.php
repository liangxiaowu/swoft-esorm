<?php


namespace Wunder\Esorm\Eloquent;
use Closure;
/**
 * Trait Must
 * @package Wunder\Esorm\Eloquent
 */
trait Must
{

    public function newMust($type, $key="", $value="")
    {
        if($type instanceof Closure){
            $bu = new Builder();
            $builder = $type($bu);
            $this->must()->where($builder->getQuery());
        }else{
            var_dump(222222);
            $this->must()->where($type, $key, $value);
        }
        return $this;
    }

    public function mustTerm(string $key, string $value="")
    {
        $this->must()->term( $key, $value);
        return $this;
    }

    public function mustTerms(string $key, $value=[])
    {
        $value = (array)$value;
        $this->must()->terms($key, $value);
        return $this;
    }

    public function mustMatch(string $key, string $value)
    {
        $this->must()->match($key, $value);
        return $this;
    }

    public function mustMatchPhrase(string $key, string $value)
    {
        $this->must()->matchPhrase($key, $value);
        return $this;
    }

    public function mustRange(string $key, array $value)
    {
        $this->must()->range($key, $value);
        return $this;
    }

}
