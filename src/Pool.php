<?php declare(strict_types=1);

namespace Wunder\Esorm;

use Swoft\Bean\BeanFactory;
use Swoft\Connection\Pool\AbstractPool;
use Swoft\Connection\Pool\Contract\ConnectionInterface;
use Wunder\Esorm\Connection\Connection;
use Wunder\Esorm\Client;

/**
 * Class Pool
 *
 * @package Wunder\Esorm
 *
 */
class Pool extends AbstractPool
{

    const DEFAULT_POOL = 'esorm.pool';

    /**
     * @var Client
     */
    private $client;

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * createConnection
     *
     * @return ConnectionInterface
     */
    public function createConnection(): ConnectionInterface
    {
        $id = $this->getConnectionId();

        /** @var Connection $connection */
        $connection  = bean(Connection::class);
        $connection->setId($id);
        $connection->setPool($this);
        $connection->setLastTime();
        $connection->setClient($this->client);
        $connection->create();
        return $connection;
    }

}
